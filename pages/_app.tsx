import Sidebar from "../components/Sidebar";
import Navbar from "../components/Navbar";

import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <div className="grid grid-cols-12 gap-6 my-14 px-5 lg:px-48 justify-between text-center sm:px-20 md:px-32">
      <div className="col-span-12 bg-white p-4 text-center lg:col-span-4 rounded-2xl">
        <Sidebar />
      </div>
      <div className="col-span-12 bg-white lg:col-span-8 rounded-2xl">
        <Navbar />
        <Component {...pageProps} />
      </div>
    </div>
  );
}

export default MyApp;

const purgecss = [
  "@fullhuman/postcss-purgecss",
  {
    content: ["./components/**/*.tsx", "./pages/**/*.tsx"],
    defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
  },
];

module.exports = {
  theme: {
    fontFamily: {
      'kausan': ['Kaushan Script'],
      'roboto': ['Roboto Slab']
     },
     extend:{
      colors: {
        brown: {
          DEFAULT: '#78350F'
        }
      }
     },
  },
   plugins: [
    'tailwindcss',
    process.env.NODE_ENV === 'production' ? purgecss : undefined,
    'postcss-preset-env',
  ],
}

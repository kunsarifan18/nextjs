import { AiFillGithub, AiFillLinkedin, AiFillInstagram } from "react-icons/ai";
import { GoLocation, GoFile } from "react-icons/go";

function Sidebar() {
  return (
    <div>
      <img
        src="/profile.jpg"
        alt="alt avatar"
        className="w-32 h-32 fill mx-auto rounded-full"
      />
      <h1 className="my-4 text-3xl font-large tracking-wider font-kausan">
        <span className="text-brown">K</span>
        unsarifan
      </h1>
      <p className="px-2 py-1 my-3 bg-gray-200 rounded-full font-roboto">
        Web Developer
      </p>
      <a
        className="flex item-center justify-center px-2 py-1 my-3 bg-gray-200 rounded-full font-roboto"
        download="name"
      >
        <GoFile className="w-6 h-6" /> Download Resume
      </a>
      {/* Social Icon */}
      <div className="flex justify-around w-9/12 mx-auto my-5 text-yellow-700 md:w-full mx-auto">
        <a href="">
          <AiFillInstagram className="w-8 h-8 cursor-pointer" />
        </a>
        <a href="">
          <AiFillLinkedin className="w-8 h-8 cursor-pointer" />
        </a>
        <a href="">
          <AiFillGithub className="w-8 h-8 cursor-pointer" />
        </a>
      </div>
      {/* Adress */}
      <div
        className="my-5 py-4 bg-gray-200"
        style={{ marginLeft: "-1rem", marginRight: "-1rem" }}
      >
        <div className="flex item-center justify-center space-x-2 font-roboto">
          <GoLocation />
          <span>Buntu, Kroya Cilacap</span>
        </div>
        <p className="my-2 font-roboto">kunsarifan18@gmail.com</p>
        <p className="my-2 font-roboto">081211981268</p>
      </div>
      {/* Email Button */}
      <button
        className="py-2 my-2 text-white bg-gradient-to-r from-yellow-600 to-yellow-800 w-8/12 rounded-full focus:outline-none font-roboto"
        onClick={() => window.open("mailto:kunsarifan18@gmail.xom")}
      >
        Email Me
      </button>
      <button className="text-white py-2 my-2 bg-gradient-to-r from-yellow-600 to-yellow-800 w-8/12 rounded-full font-roboto">
        Toggle Theme
      </button>
    </div>
  );
}

export default Sidebar;

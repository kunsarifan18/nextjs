import { useState } from "react";
import Link from "next/link";

function Navbar() {
  const [activeItem, setActiveItem] = useState<string>();
  //   useEffect(() => {

  //   });
  return (
    <div>
      <span className="font-bold text-yellow">{activeItem}</span>
      <div className="text-yellow font-lg text-brown flex space-x-3">
        {activeItem !== "About" && (
          <Link href="/">
            <a href="">
              <span onClick={() => setActiveItem("About")}>About</span>
            </a>
          </Link>
        )}
        {activeItem !== "Projects" && (
          <Link href="/projects">
            <a href="">
              <span onClick={() => setActiveItem("Projects")}>Projects</span>
            </a>
          </Link>
        )}
        {activeItem !== "Resume" && (
          <Link href="/resume">
            <a href="">
              <span onClick={() => setActiveItem("Resume")}>Resume</span>
            </a>
          </Link>
        )}
      </div>
    </div>
  );
}

export default Navbar;
